import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>{{title}}</h1>
    <h2>Service Strategies</h2>
    <fibonacci></fibonacci>
    
    <h2>Using Values</h2>
    <using-values></using-values>
  `,
  styles: []
})
export class AppComponent {
  title = 'Services Demo';
}
