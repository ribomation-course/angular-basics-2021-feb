import {Directive, Input} from '@angular/core';
import {FormControl, NG_VALIDATORS,
  ValidationErrors, Validator
} from '@angular/forms';
import {multipleOf}   from './user-validators';

@Directive({
  selector:  '[multipleOf]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: MultipleOfDirective, multi: true}
  ]
})
export class MultipleOfDirective implements Validator {
  @Input('multipleOf') n: number = 2;

  validate(field: FormControl): ValidationErrors | null {
    return multipleOf(this.n)(field);
  }
}


