import {ProductsPage} from './pages/products.page';

describe('Page: Products', () => {
  let page: ProductsPage;

  beforeEach(() => {
    page = new ProductsPage();
    page.navigateTo();
  });

  it('should display header', () => {
    expect(page.getHeaderText()).toContain('Products');
  });

  it('should have three product rows', () => {
    page.getProductTableRows().then(rows => {
      expect(rows.length).toBe(3);
    })
  });

});
