import {Component} from "@angular/core";
import {AuthService} from "./auth.service";
import {Router} from "@angular/router";

@Component({
  selector: "app-login",
  template: `
    <div>
      <span *ngIf="msg">{{msg}} <br/></span>
      Username: <input type="text" [(ngModel)]="data.usr"/> <br/>
      Password: <input type="password" [(ngModel)]="data.pwd"/> <br/>
      <button type="button" (click)="login()">Login</button>
      DATA: {{data | json}}
    </div>
  `,
  styles: [`span {
    color: red;
  }`]
})
export class LoginComponent {
  data: any = {usr: '', pwd: ''};
  msg: string;

  constructor(private auth: AuthService, private router:Router) {}

  login(): void {
    console.log("[auth]", "login", this.data.usr, this.data.pwd);
    this.msg = null;
    if (this.data.usr && this.data.pwd) {
      this.auth.login(this.data.usr, this.data.pwd);
      this.data.usr = this.data.pwd = "";
      this.router.navigate(["/first"]);
    } else {
      this.msg = "Must fill in both username and password";
    }
  }
}
