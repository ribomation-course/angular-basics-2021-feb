import {Component} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";

@Component({
  selector: "app-fake-data",
  template: `
    <div>
      <h1>{{title | uppercase}}</h1>
      <p>demo of static route data</p>
    </div>
  `,
  styles: []
})
export class FakeDataComponent {
  title: string = 'no title';
  constructor(private route: ActivatedRoute) {
    route.data
      .map(d => d.title)
      .subscribe(t => this.title = t);
  }
}
