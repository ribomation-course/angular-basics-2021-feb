import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>{{title}}</h1>
    <h2>*ngIf</h2>
    <app-demo-ngif></app-demo-ngif>
    <h2>*ngIf using template block</h2>
    <app-demo-ngif-2></app-demo-ngif-2>
    <h2>ngSwitch</h2>
    <app-demo-ngswitch></app-demo-ngswitch>
    <h2>ngFor</h2>
    <app-demo-ngfor></app-demo-ngfor>
    <h2>ngFor using variables</h2>
    <app-demo-ngfor-2></app-demo-ngfor-2>
    <h2>ngClass</h2>
    <app-demo-ngclass></app-demo-ngclass>
    <h2>ngStyle</h2>
    <app-demo-ngstyle></app-demo-ngstyle>
    
    <h2>Exercise: Directives</h2>
    <exercise-directives></exercise-directives>
    
    <h2>JSON</h2>
    <app-demo-json></app-demo-json>
    <h2>Async</h2>
    <app-demo-async></app-demo-async>
    <h2>Text Transformation</h2>
    <app-demo-text-transformations></app-demo-text-transformations>
    <h2>Number Formats</h2>
    <app-demo-number-formats></app-demo-number-formats>
    <h2>Sub List</h2>
    <app-demo-sublist></app-demo-sublist>
    <h2>Date Formats</h2>
    <app-demo-date-formats></app-demo-date-formats>
    <h2>Train_Case</h2>
    <app-demo-traincase></app-demo-traincase>
    
    <h2>Number Stepper</h2>
    <app-demo-number-stepper></app-demo-number-stepper>
    
    <div style="margin-top: 4rem;"></div>
  `,
  styles: [
    `h2 {border-top: solid 1px gray}`
  ]
})
export class AppComponent {
  title = 'Demo of ng Directives';
}
