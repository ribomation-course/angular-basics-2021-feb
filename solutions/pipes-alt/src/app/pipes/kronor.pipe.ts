import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'kronor'
})
export class KronorPipe implements PipeTransform {

    transform(value: number, decimals: boolean = false): string {
        let result = Number(value).toLocaleString('sv-SE', {
            useGrouping: true,
            minimumFractionDigits: decimals ? 2 : 0,
            maximumFractionDigits: decimals ? 2 : 0,
        });

        const NBSP = '\u00A0'
        result = result + NBSP + 'kr'

        return result;
    }

}
