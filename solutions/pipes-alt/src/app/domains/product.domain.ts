export class Product {
    constructor(
        public name: string,
        public isService: boolean,
        public price: number,
        public itemsInStore: number,
        public lastUpdate: Date
    ) {
    }

    static create() {
        const name = Product.NAMES[Math.floor(Math.random() * Product.NAMES.length)]
        const isService = Math.random() < .4
        const price = 5 + Math.random() * 100000
        const itemsInStore = Math.random() * 50
        const days = 1 + Math.floor(Math.random() * 30)
        const MS_PER_DAY = 24 * 3600 * 1000
        const lastUpdate = new Date(Date.now() - days * MS_PER_DAY)

        return new Product(name, isService, price, itemsInStore, lastUpdate)
    }

    private static NAMES: string[] = [
        'äpple', 'banan', '´kokos nör', 'dadlar', 'apelsin',
        'päron', 'kiwi', 'flaggstångsknopskåpefärg'
    ];
}
