import {Component, OnInit} from '@angular/core';
import {Product} from "./domains/product.domain";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    count: number = 5
    products: Product[] = []

    ngOnInit(): void {
        this.update()
    }

    update() {
        this.products = []
        for (let k = 0; k < this.count; ++k) {
            this.products.push( Product.create())
        }
    }

}
