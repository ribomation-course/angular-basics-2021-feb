import {Component} from '@angular/core';
import {Person} from "./domain/person.domain";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    person = Person.mk();

    onEditableChanged(payload:any) {
        console.log('onEditableChanged: %o', payload)
    }
}
