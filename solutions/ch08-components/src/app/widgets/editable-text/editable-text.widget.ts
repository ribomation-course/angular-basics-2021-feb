import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

export interface PropertyUpdated {
    key: string;
    value: string | number;
}

@Component({
    selector: 'editable-text',
    templateUrl: './editable-text.widget.html',
    styleUrls: ['./editable-text.widget.scss']
})
export class EditableTextWidget implements OnInit {
    @Input('value') valueOrig: string | number = '';
    @Input('property') propertyName: string = '';
    @Input('placeholder') placeholder = 'Click to edit'
    @Output('updated') updateEmitter = new EventEmitter<PropertyUpdated>();
    formVisible = false;
    valueForm: string | number = '';

    constructor() {
    }

    ngOnInit(): void {
    }

    showForm() {
        this.valueForm = this.valueOrig;
        this.formVisible = true
    }

    get hasValue(): boolean {
        return this.valueOrig !== null
            && this.valueOrig !== undefined
            && this.valueOrig.toString().trim().length > 0;
    }

    onKeyEvent(ev: KeyboardEvent) {
        // console.log('** onKeyEvent: %o', ev)
        if (ev.key === 'Escape') {
            this.cancel();
        } else if (ev.key === 'Enter') {
            this.save();
        }
    }

    cancel() {
        this.formVisible = false;
        this.valueForm = ''
    }

    save() {
        this.formVisible = false;
        this.valueOrig = this.valueForm;
        this.valueForm = ''
        this.updateEmitter.emit({
            key: this.propertyName,
            value: this.valueOrig
        });
    }

}
