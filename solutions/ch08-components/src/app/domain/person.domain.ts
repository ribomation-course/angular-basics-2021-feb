export class Person {
    constructor(
        public name: string,
        public age: number,
        public city: string,
        public email: string
    ) {
    }

    static mk() {
        return new Person('Anna Conda', 42, 'Sundsvall', 'anna@gmail.com')
    }
}

