import {Component, OnInit} from '@angular/core';
import {LoggerService} from '../../services/logger.service';

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.page.html',
    styleUrls: ['./welcome.page.scss']
})
export class WelcomePage implements OnInit {
    readonly image = '/assets/img/store.jpg';

    constructor(private log: LoggerService) {
    }

    ngOnInit(): void {
        this.log.debug('this is the welcome page')
    }

}
