import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Product} from '../domain/product.domain';

@Injectable()
export class FakeProductsService {

    findAll(): Observable<Product[]> {
        return of<Product[]>(
            [
                {
                    'id': 'qreqwtqwerwertwert',
                    'name': 'bread',
                    'price': 20,
                    'image': '/assets/img/bread.jpg'
                },
                {
                    'id': 'vzcvzcvzxcvzxcv',
                    'name': 'meat',
                    'price': 80,
                    'image': '/assets/img/meat.jpg'
                }
            ]
        );
    }
}
