import {Injectable} from '@angular/core';
import {environment as env} from '../../environments/environment';


@Injectable({providedIn: 'root'})
export class LoggerService {
    verbose: boolean = env.debugLogging;

    debug(msg: string) {
        if (this.verbose) {
            const ts = new Date().toLocaleTimeString();
            console.log('[debug] %s: %s', ts, msg);
        }
    }

}

