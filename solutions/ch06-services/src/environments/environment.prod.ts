export const environment = {
    production: true,
    debugLogging: false,
    backendUrl: 'https://api.ribomation.se/api/v1/products'
};
