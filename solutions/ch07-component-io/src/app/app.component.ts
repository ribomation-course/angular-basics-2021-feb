import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    fontSize = 2;
    message = 'Foobar STRIKES aGaIn !!';
}
