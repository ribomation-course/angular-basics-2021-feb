import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector: 'number-stepper',
    templateUrl: './number-stepper.widget.html',
    styleUrls: ['./number-stepper.widget.css']
})
export class NumberStepperWidget {
    @Input('value') currentValue: number | string = 0;
    @Output('valueChange') valueEmitter = new EventEmitter<number>();
    @Input('min') minValue = 1;
    @Input('max') maxValue = 10;

    dec() {
        this.update(-1);
    }

    inc() {
        this.update(+1);
    }

    update(delta: number) {
        this.currentValue = Number(this.currentValue) + delta;
        if (this.currentValue < this.minValue) {
            this.currentValue = this.minValue;
        }
        if (this.currentValue > this.maxValue) {
            this.currentValue = this.maxValue;
        }
        this.valueEmitter.emit(this.currentValue);
    }
}
