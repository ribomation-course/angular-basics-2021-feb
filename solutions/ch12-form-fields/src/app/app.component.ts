import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    today = new Date();
    offset = 5;

    nextDate(d: Date, n: number): Date {
        const DAY_MS = 24 * 3600 * 1000;
        return new Date(d.getTime() + n * DAY_MS);
    }

    toISO(d: Date): string {
        return d.toISOString().substring(0, 10);
    }

    fromISO(d: string): Date {
        return new Date(d);
    }

}
