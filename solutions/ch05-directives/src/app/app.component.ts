import {Component} from '@angular/core';
import {Person} from "./domain/person.domain";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    persons: Person[] = [];

    toggle() {
        if (this.persons.length === 0) {
            this.persons = this.allPersons;
        } else {
            this.persons = []
        }
    }

    allPersons: Person[] = [
        {
            "firstName": "Marcus",
            "lastName": "Overnell",
            "email": "movernell0@chronoengine.com"
        }, {
            "firstName": "Thoma",
            "lastName": "Wilkisson",
            "email": "twilkisson1@vk.com"
        }, {
            "firstName": "Gerri",
            "lastName": "Paley",
            "email": "gpaley2@weibo.com"
        }, {
            "firstName": "Halie",
            "lastName": "Rawne",
            "email": "hrawne3@google.co.uk"
        }, {
            "firstName": "Dulcinea",
            "lastName": "Sambell",
            "email": "dsambell4@salon.com"
        }, {
            "firstName": "Pauly",
            "lastName": "De Ruggiero",
            "email": "pderuggiero5@dagondesign.com"
        }, {
            "firstName": "Marney",
            "lastName": "Pinchen",
            "email": "mpinchen6@github.com"
        }, {
            "firstName": "Catha",
            "lastName": "Fosberry",
            "email": "cfosberry7@jalbum.net"
        }, {
            "firstName": "Timofei",
            "lastName": "Spurriar",
            "email": "tspurriar8@pbs.org"
        }, {
            "firstName": "Roderich",
            "lastName": "Fitzackerley",
            "email": "rfitzackerley9@nydailynews.com"
        }]


}
