import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HomePage} from './pages/home/home.page';
import {AboutPage} from './pages/about/about.page';
import {ProductsPage} from './pages/products/products.page';
import {Route, RouterModule} from '@angular/router';

const routes: Route[] = [
    {path: 'home', component: HomePage},
    {path: 'products', component: ProductsPage},
    {path: 'about', component: AboutPage},
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: '**', redirectTo: '/home', pathMatch: 'full'},
];

@NgModule({
    declarations: [
        AppComponent,
        HomePage,
        AboutPage,
        ProductsPage
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
