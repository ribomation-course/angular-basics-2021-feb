import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Person} from '../../domain/person.domain';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-component-oriented-form',
    templateUrl: './component-oriented-form.widget.html',
    styleUrls: ['../form-styles.scss']
})
export class ComponentOrientedFormWidget implements OnInit {
    @Input('init') person: Person = new Person('', '');
    @Output('updated') submitEmitter = new EventEmitter<Person>();

    form: FormGroup;
    firstNameCtrl: FormControl;
    emailCtrl: FormControl;

    constructor() {
        this.firstNameCtrl = new FormControl('', [Validators.required, Validators.minLength(4)]);
        this.emailCtrl = new FormControl('', [Validators.required, Validators.email]);
        this.form = new FormGroup({
            firstName: this.firstNameCtrl,
            email: this.emailCtrl
        });
    }

    ngOnInit(): void {
        this.firstNameCtrl.setValue(this.person.firstName);
        this.emailCtrl.setValue(this.person.email);
    }

    onSubmit() {
        this.submitEmitter.emit(this.form.value);
    }
}
