import {Component, Input, OnInit, Output, EventEmitter, ViewChild} from '@angular/core';
import {Person} from '../../domain/person.domain';
import {FormGroup} from '@angular/forms';

@Component({
    selector: 'app-template-oriented-form',
    templateUrl: './template-oriented-form.widget.html',
    styleUrls: ['../form-styles.scss']
})
export class TemplateOrientedFormWidget implements OnInit {
    @Input('init') person: Person = new Person('', '');
    @Output('updated') submitEmitter = new EventEmitter<Person>();

    @ViewChild('form') form: FormGroup | undefined;

    constructor() {
    }

    ngOnInit(): void {
        if (!this.person) {
            throw new Error('missing value for [init]');
        }
    }

    onSubmit() {
        this.submitEmitter.emit(this.person);
    }

}
