import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TemplateOrientedFormWidget } from './widgets/template-oriented-form/template-oriented-form.widget';
import { ComponentOrientedFormWidget } from './widgets/component-oriented-form/component-oriented-form.widget';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TemplateOrientedFormWidget,
    ComponentOrientedFormWidget
  ],
  imports: [
    BrowserModule, FormsModule, ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
