"use strict";
exports.__esModule = true;
exports.Account = void 0;
var Account = /** @class */ (function () {
    function Account(accno, balance) {
        this.accno = accno;
        this.balance = balance;
    }
    return Account;
}());
exports.Account = Account;
//let a1 = new Account('1234', 100)
//console.log('a1: %o', a1);
