"use strict";
exports.__esModule = true;
var account_1 = require("./account");
var accounts = [
    new account_1.Account('qwerty', 100),
    new account_1.Account('abc', 200),
    new account_1.Account('zxcvb', 300),
];
for (var _i = 0, accounts_1 = accounts; _i < accounts_1.length; _i++) {
    var a = accounts_1[_i];
    console.log(a);
}
