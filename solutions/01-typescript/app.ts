import {Account} from './account'

let accounts = [
    new Account('qwerty', 100),
    new Account('abc', 200),
    new Account('zxcvb', 300),
]

for (const a of accounts) {
    console.log(a);
}

