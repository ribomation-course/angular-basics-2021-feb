import {Component, OnInit} from '@angular/core';
import {User} from './domain/user.domain';
import {UsersService} from './services/users.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    users: User[] = [
        new User(1, 'Nisse', 'Hult', 1968)
    ];
    user: User | undefined = undefined;
    row: number = -1;

    constructor(public userSvc: UsersService) {
    }

    ngOnInit(): void {
        this.userSvc
            .findAll()
            .subscribe(objs => {
                this.users = objs;
            });
    }

    cancel(): void {
        this.user = undefined;
        this.row = -1;
    }

    create(): void {
        this.user = new User();
        this.user.birthYear = 1980;
    }

    edit(id: number, row: number): void {
        this.userSvc
            .findById(id)
            .subscribe(obj => {
                this.user = obj;
                this.row = row;
            });
    }

    save(): void {
        if (!this.user) {
            return;
        }

        if (this.user?.id === -1) {
            this.userSvc
                .create(this.user)
                .subscribe(obj => {
                    this.users.push(obj);
                    this.cancel();
                });
        } else {
            this.userSvc
                .update(this.user.id, this.user)
                .subscribe(obj => {
                    this.users.splice(this.row, 1, obj);
                    this.cancel();
                });
        }
    }

    remove(id: number, row: number): void {
        this.userSvc
            .remove(id)
            .subscribe(ok => {
                this.users.splice(row, 1);
            });
    }
}
