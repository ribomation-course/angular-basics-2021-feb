import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../domain/user.domain';
import {map, tap} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class UsersService {
    readonly baseUrl = 'http://localhost:3000/users';

    constructor(private httpSvc: HttpClient) {
    }

    findAll(): Observable<User[]> {
        return this.httpSvc
            .get<User[]>(this.baseUrl)
            .pipe(
                map((lst: any[]) => lst.map(obj => User.fromServer(obj))),
                tap(payload => console.debug('[user-svc] findAll: %o', payload))
            );
    }

    findById(id: number): Observable<User> {
        return this.httpSvc
            .get<User>(`${this.baseUrl}/${id}`)
            .pipe(
                map(obj => User.fromServer(obj)),
                tap(payload => console.debug('[user-svc] findById(%d): %o', id, payload))
            );
    }

    create(user: User): Observable<User> {
        return this.httpSvc
            .post<User>(`${this.baseUrl}`, user.toServer())
            .pipe(
                map(obj => User.fromServer(obj)),
                tap(obj => console.debug('[user-svc] create(): %o', obj))
            );
    }

    update(id: number, partial: User): Observable<User> {
        return this.httpSvc
            .put<User>(`${this.baseUrl}/${id}`, partial.toServer())
            .pipe(
                map(obj => User.fromServer(obj)),
                tap(obj => console.debug('[user-svc] update(%d): %o', id, obj))
            );
    }

    remove(id: number): Observable<void> {
        return this.httpSvc
            .delete<void>(`${this.baseUrl}/${id}`);
    }

}
