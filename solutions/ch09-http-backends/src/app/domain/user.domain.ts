export class User {
    constructor(
        public id: number = -1,
        public fname: string = '',
        public lname: string = '',
        public birthYear: number = 0,
    ) {
    }

    get name(): string {
        return `${this.fname} ${this.lname}`;
    }

    get age(): number {
        return new Date().getFullYear() - this.birthYear;
    }

    static fromServer(payload: any): User {
        return Object.assign(new User(), payload);
    }

    toServer(): any {
        return Object.assign({}, {
            fname: this.fname,
            lname: this.lname,
            birthYear: this.birthYear,
        });
    }

}

