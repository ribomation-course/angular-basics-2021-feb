import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ProductsService} from "../../services/products.service";
import {Product} from "../../domain/product.domain";

@Component({
    selector: 'app-product-view',
    templateUrl: './product-view.page.html',
    styleUrls: ['./product-view.page.scss']
})
export class ProductViewPage implements OnInit {
    product: Product | undefined;

    constructor(
        private route: ActivatedRoute,
        private productSvc: ProductsService
    ) {
    }

    ngOnInit(): void {
        const id = Number(this.route.snapshot.paramMap.get('id'));
        this.product = this.productSvc.findById(id);
    }

}
