import {Component, OnInit} from '@angular/core';
import {Product} from "../../domain/product.domain";
import {ProductsService} from "../../services/products.service";

@Component({
    selector: 'app-products',
    templateUrl: './products.page.html',
    styleUrls: ['./products.page.scss']
})
export class ProductsPage implements OnInit {
    products: Product[] = [];

    constructor(private productSvc: ProductsService) {
        this.products = productSvc.findAll();
    }

    ngOnInit(): void {
    }

}
