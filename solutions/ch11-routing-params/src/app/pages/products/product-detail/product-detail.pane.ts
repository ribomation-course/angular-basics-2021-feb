import {Component, OnInit} from '@angular/core';
import {Product} from "../../../domain/product.domain";
import {ActivatedRoute} from "@angular/router";
import {ProductsService} from "../../../services/products.service";
import {map} from "rxjs/operators";

@Component({
    selector: 'app-product-detail',
    templateUrl: './product-detail.pane.html',
    styleUrls: ['./product-detail.pane.scss']
})
export class ProductDetailPane implements OnInit {
    product: Product | undefined;

    constructor(
        private route: ActivatedRoute,
        private productSvc: ProductsService
    ) {
    }

    ngOnInit(): void {
        this.route
            .paramMap
            .pipe(
                map(params => params.get('id')),
                map(id => Number(id))
            )
            .subscribe(id => {
                this.product = this.productSvc.findById(id);
            })
    }

}
