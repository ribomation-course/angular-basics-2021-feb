import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HomePage} from './pages/home/home.page';
import {AboutPage} from './pages/about/about.page';
import {ProductsPage} from './pages/products/products.page';
import {Route, RouterModule} from '@angular/router';
import {ProductViewPage} from './pages/product-view/product-view.page';
import {ProductDetailPane} from './pages/products/product-detail/product-detail.pane';

const routes: Route[] = [
    {path: 'home', component: HomePage},
    {path: 'about', component: AboutPage},

    {
        path: 'products', component: ProductsPage, children: [
            {path: 'detail/:id', component: ProductDetailPane},
        ]
    },
    {path: 'product-view/:id', component: ProductViewPage},

    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: '**', redirectTo: '/home', pathMatch: 'full'},
];

@NgModule({
    declarations: [
        AppComponent,
        HomePage,
        AboutPage,
        ProductsPage,
        ProductViewPage,
        ProductDetailPane
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
