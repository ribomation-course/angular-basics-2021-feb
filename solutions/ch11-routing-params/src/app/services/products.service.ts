import {Injectable} from '@angular/core';
import {Product} from "../domain/product.domain";

@Injectable({
    providedIn: 'root'
})
export class ProductsService {
    products: Product[] = [
        new Product("äpple", 2),
        new Product("banan", 3),
        new Product("apelsin", 4),
        new Product("kiwi", 7),
        new Product("mango", 10),
        new Product("päron", 1),
        new Product("citron", 6),
    ];

    findAll(): Product[] {
        return this.products;
    }

    findById(id: number) {
        return this.products[id];
    }

}
