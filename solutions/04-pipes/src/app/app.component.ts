import {Component, OnInit} from '@angular/core';
import {Product} from './product.domain';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    numProducts = 8;
    products: Product[] = [];

    ngOnInit(): void {
        this.generate();
    }

    generate() {
        this.products = [];
        for (let k = 0; k < this.numProducts; ++k) {
            this.products.push(Product.create());
        }
    }

}
